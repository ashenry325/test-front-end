# test-front-end

# Frontend Developer
1.	Do you prefer vuejs or reactjs? Why ?
2.	What complex things have you done in frontend development ?
3.	why does a UI Developer need to know and understand UX? how far do you understand it?
4.	Give your analysis results regarding https://taskdev.mile.app/login from the UI / UX side!
5.	Create a better login page based on https://taskdev.mile.app/login and deploy on https://www.netlify.com !
6.	Solve the logic problems below !


# Logic
6.a Swap the values of variables A and B
// terdapat 2 variabel A & B
A = 3
B = 5

// Tukar Nilai variabel A dan B, Syarat Tidak boleh menambah Variabel Baru

// Hasil yang diharapkan :
A = 5
B = 3

6.b Find the missing numbers from 1 to 100
```sh
numbers = [1, 2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100];
```
        

6.c return the number which is called more than 1
```sh
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 25, 25, 25, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 34, 34, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 83, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 92, 93, 94, 95, 96, 97, 98, 99, 100];
```


6.d 
```sh
array_code = ["1.", "1.1.", "1.2.", "1.3.", "1.4.", "1.1.1.", "1.1.2.", "1.1.3.", "1.2.1.", "1.2.2.", "1.3.1.", "1.3.2.", "1.3.3.", "1.3.4.", "1.4.1.", "1.4.3."]
```

// return :
```sh
object_code = 
        {
        "1":{
            "1":{
                "1":"1.1.1.", 
                "2":"1.1.2.", 
                "3":"1.1.3."
            }, 
            "2":{
                "1":"1.2.1.", 
                "2":"1.2.2."
            }, 
            "3":{
                "1":"1.3.1.", 
                "2":"1.3.2.", 
                "4":"1.3.4."
            }, 
            "4":{
                "1":"1.4.1.", 
                "3":"1.4.3."
            }
         }
```


## Answer
1)	I prefer to react because of the high speed and community
2)	processing data rest api into a desired content such as pagination, layout and others
3)	because indeed to deliver our application to the client we also need to consider whether our application is easy to use so that the user can understand more about using it and our application must be pleasing to the eye so that the user doesn't get bored of using our application
4)	I am not too deep into ui / ux but according to what I experience and I feel the login menu is very good and informative and the layout is in accordance with the corner of the eye when looking at it but if possible give suggestions maybe from the hedear you can change the backround-color to # 2460da to better fit the image
5)	https://gallant-volhard-91f690.netlify.app/

6.a)
```sh
let a = 5;
let b = 3;
[a, b] = [b, a]

console.log(a)
console.log(b)
```

6.b)
```sh
let numArray = [1, 2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100];


// cara 1
function missingNumbers1(dataArray) {
    let data = dataArray.reduce((acc, cur, idx, arr) => {
        let dataHilang = cur - arr[idx - 1];
        if (dataHilang > 1) {
            for (i = 1; i < dataHilang; i++) {
                acc.push(arr[idx - 1] + i);
            }
        }
        return acc;
    }, [])

    return data
}

// cara 2
function missingNumbers2(dataArray) {
    const missing = [];

    for (let i in dataArray) {
        let x = dataArray[i] - dataArray[i - 1];
        let diff = 1;
        while (diff < x) {
            missing.push(dataArray[i - 1] + diff);
            diff++;
        }
    }
    return missing
}
console.log(missingNumbers1(numArray))
console.log(missingNumbers2(numArray))
```

6.c)
```sh
let duplicatedArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 25, 25, 25, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 34, 34, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 83, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 92, 93, 94, 95, 96, 97, 98, 99, 100];

function findDuplicateArray(arra1) {
    const object = {};
    const result = [];

    arra1.map(item => {
        if (!object[item])
            object[item] = 0;
        object[item] += 1;
    })

    for (const prop in object) {
        if (object[prop] >= 2) {
            result.push(prop);
        }
    }

    return result;
}
console.log(findDuplicateArray(duplicatedArray))
```